# Configuration design
To keep the API key secure and avoid exposing it in the code, we use the following configuration design:

1. Create a `.env` file in the project's root directory, and add the API key in the following format: API_KEY=your_openai_api_key_here

2. Add `.env` to the `.gitignore` file to ensure it is not tracked by Git and accidentally pushed to the repository.

3. Install the `python-dotenv` library if you haven't already:

```bash
pip install python-dotenv
```
4. In your Python code, use the following snippet to load the .env file and access the API key:

```
import os
from dotenv import load_dotenv

load_dotenv()
api_key = os.environ.get("API_KEY")
```

By following these steps, the API key will be securely stored in the .env file, which will not be included in the Git repository.
